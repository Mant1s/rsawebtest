<?php

namespace RSA\WebTestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('RSAWebTestBundle:Default:index.html.twig', array('name' => 'admin'));
    }
}
