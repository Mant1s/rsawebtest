<?php
// src/Jimmy/GymBundle/DataFixtures/ORM/LoadUserData.php

namespace RSA\WebTestBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RSA\WebTestBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setEnabled(true);
        $userAdmin->setRoles(array('ROLE_SUPER_ADMIN'));
        $userAdmin->setUsername('admin');
        $userAdmin->setEmail('admin@admin.co.za');
        $userAdmin->setPlainPassword('test');
        $userAdmin->setFullName('Jane');
        $userAdmin->setBirthDate('today');

        $manager->persist($userAdmin);
        $manager->flush();
    }
}